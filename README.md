# `simple-plist`

[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/joe.wollard/node-simple-plist/main?logo=gitlab&style=plastic)](https://gitlab.com/joe.wollard/node-simple-plist/-/pipelines/latest)
[![npm](https://img.shields.io/npm/dm/simple-plist?logo=npm&style=plastic)](https://www.npmjs.org/package/simple-plist)
[![npm](https://img.shields.io/npm/v/simple-plist?logo=npm&style=plastic)](https://www.npmjs.com/package/simple-plist)
[![npm](https://img.shields.io/librariesio/release/npm/simple-plist?logo=npm&style=plastic)](https://www.npmjs.com/package/simple-plist)

A simple API for interacting with binary and plain text plist data.

## Installation

```sh
# with yarn
yarn add simple-plist

# or with npm
npm install simple-plist
```

## Reading Data

```js
import Plist from "simple-plist";

async function foo() {
  const data = await Plist.read("/path/to/some.plist");
  console.log(data);
}
```

## Writing Data

```js
import Plist from "simple-plist";

async function foo() {
  const myPlist = { foo: "bar" };
  // Write data to xml file
  await Plist.write({
    file: "/path/to/xml.plist",
    input: myPlist,
  });

  // Write data to binary file
  await Plist.write({
    binary: true,
    file: "/path/to/xml.plist",
    input: myPlist,
  });
}
```

## In-Memory Conversion

```js
import Plist from "simple-plist";

async function foo() {
  // Convert a Javascript object to a plist xml string
  const xml = Plist.stringify({ name: "Joe", answer: 42 });

  // Convert a plist xml string or a binary plist buffer to a Javascript object
  const data = await parse(
    "<plist><dict><key>name</key><string>Joe</string></dict></plist>"
  );
}
```

## TypeScript Generics

If you're using TypeScript, the `parse` and `read` methods both support
generics so you don't lose type information. If no type is specified, the
default of `PlistValue` will be used.

```ts
import Plist from "simple-plist";

interface Settings {
  showToolbar?: boolean;
  avatar?: string;
}

async function loadSettings() {
  return Plist.read<MyDatabase>("/path/to/settings.plist");
}
```
