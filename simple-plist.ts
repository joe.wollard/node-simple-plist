import bplistCreator from "bplist-creator";
import bplistParser from "bplist-parser";
import fs, { WriteFileOptions } from "fs";
import plist, { PlistValue } from "plist";
import { URL } from "url";
import { promisify } from "util";

const read = promisify(fs.readFile);
const write = promisify(fs.writeFile);

export namespace SimplePlist {
  export type File = string | number | Buffer | URL;

  export interface ParseArgs {
    input: string | Buffer;
    filePath?: string;
  }

  export interface WriteArgs {
    /**
     * Write in binary mode when true.
     * @default false
     */
    binary?: boolean;
    /** The file path or buffer to write to */
    file: File;
    /** Any plist object */
    input: PlistValue;
    writeOptions?: WriteFileOptions;
  }
}

export default class Plist {
  /**
   * Reads the `input` string or Buffer and parses the contents, automatically
   * choosing between binary and plain text parse modes.
   */
  static parse<T = PlistValue>({ input, filePath }: SimplePlist.ParseArgs) {
    const firstByte = input[0];
    let results: T;

    if (firstByte === 60 || firstByte === "<") {
      results = (plist.parse(input.toString()) as unknown) as T;
    } else if (firstByte === 98) {
      [results] = bplistParser.parseBuffer(input);
    } else if (filePath) {
      throw new Error(`Unable to determine format for '${filePath}'`);
    } else {
      throw new Error("Unable to determine format of plist");
    }

    return results;
  }

  /**
   * Reads the contents of `file` from disk and returns the parsed content.
   */
  static async read<T = PlistValue>(file: SimplePlist.File) {
    const content = await read(file);
    if (content.length === 0) {
      return {} as T;
    }
    return Plist.parse<T>({ input: content, filePath: file as string });
  }

  /**
   * Writes the `input` data to the `file` specified, optionally in `binary`
   * format.
   */
  static async write({
    binary = false,
    file,
    input,
    writeOptions,
  }: SimplePlist.WriteArgs) {
    const data = binary ? bplistCreator(input) : Plist.stringify(input);
    return write(file, data, writeOptions);
  }

  /**
   * Converts an object to a plist string
   */
  static stringify(input: PlistValue) {
    return plist.build(input);
  }
}
