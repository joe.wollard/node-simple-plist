import Plist from "../simple-plist";
import { unlink } from "fs";

const filePath = `${__dirname}/write-test-binary1.plist`;
const testObj = {
  Name: "John Doe",
  "Birth Year": 1942,
  "Travel Log": [
    "Tokyo, Honshu, Japan",
    "Philadelphia, PA",
    "Recife, Pernambuco, Brazil",
  ],
};

describe("Writing files", () => {
  afterEach(() => {
    return new Promise((resolve, reject) => {
      unlink(filePath, function (err) {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  });

  it("can write to binary file", async () => {
    await Plist.write({ binary: true, file: filePath, input: testObj });
    const contents = await Plist.read(filePath);
    expect(contents).toMatchObject(testObj);
  });

  it("can write to xml file", async () => {
    await Plist.write({ file: filePath, input: testObj });
    const contents = await Plist.read(filePath);
    expect(contents).toMatchObject(testObj);
  });
});
