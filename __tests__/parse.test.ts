import Plist from "../simple-plist";

describe("String parsing", () => {
  it("can parse a string", async () => {
    const doc = await Plist.read(`${__dirname}/test-binary1.plist`);
    const plistString = Plist.stringify(doc);
    const parsedDoc = Plist.parse({ input: plistString });

    expect(parsedDoc).toEqual(doc);
  });
});
