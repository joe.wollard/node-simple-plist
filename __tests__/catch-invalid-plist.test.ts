import Plist from "../simple-plist";

describe("Catch Invalid Plists", () => {
  it("Throws an error on improperly formatted plist file", async () => {
    try {
      await Plist.read(`${__dirname}/test-xml1-invalid.plist`);
      fail();
    } catch (error) {
      expect(error).toBeTruthy;
    }
  });

  it("Throws an error on improperly formatted plist string", async () => {
    try {
      Plist.parse({ input: "🙄" });
      fail();
    } catch (error) {
      expect(error.message).toEqual("Unable to determine format of plist");
    }
  });

  it("returns an empty object when the file is zero bytes", async () => {
    const obj = await Plist.read(`${__dirname}/test-xml1-invalid-2.plist`);
    expect(obj).toEqual({});
  });
});
