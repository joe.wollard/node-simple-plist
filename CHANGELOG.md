# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.0.0-rc.0](https://gitlab.com/joe.wollard/node-simple-plist/compare/v1.0.0...v2.0.0-rc.0) (2020-07-26)


### ⚠ BREAKING CHANGES

* Names of some functions have changed / all functions are now static methods of `Plist` namespace
* **node:** node@6 is no longer supported

### Features

* add typescript support ([753ab55](https://gitlab.com/joe.wollard/node-simple-plist/commit/753ab5578bc41a9097478b26322d5745b5cb95cd)), closes [#33](https://gitlab.com/joe.wollard/node-simple-plist/issues/33)


* **node:** drop support for node@6 ([3546eb0](https://gitlab.com/joe.wollard/node-simple-plist/commit/3546eb00a83c3db269da8584b9817ab7a6cdb1d6))
